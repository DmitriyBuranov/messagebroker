﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.EventConsumers
{
    public class GivePromoCodesToCustomersWithPreferenceEventConsumer : IConsumer<IReceivePromocode>
    {
        private readonly IPromocodeService _promoCodesService;

        public GivePromoCodesToCustomersWithPreferenceEventConsumer(
            IPromocodeService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<IReceivePromocode> context)
        {
            await _promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
