﻿using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Events;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Mappers;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromocodeService : IPromocodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly ILogger<PromocodeService> _logger;

        public PromocodeService(
            ILogger<PromocodeService> logger,
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            _logger = logger;
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        public async Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(IReceivePromocode message)
        {
            var preference = await _preferencesRepository.GetByIdAsync(message.PreferenceId);

            if (preference is null)
            {
                _logger.LogError($"Preference not found. Id: {message.PreferenceId}");
                return false;
            }

            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(message, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return true;
        }
    }
}
